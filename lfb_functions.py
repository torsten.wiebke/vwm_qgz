from operator import and_
from qgis.core import *
from qgis.gui import *
from time import sleep
import pandas as pd

measure = []
measures = []


@qgsfunction(args='auto', group='Custom', referenced_columns=[])
def gps_position(feature, parent):
    """
    Reads GPS Info
    usage gps_position() -> string mit longitude, latitude, hdop,vdop, elevation, satellites    
    """
    val0 = feature['gps_info']
    if (val0=='') or (val0 is None): #nur wenn Wert NULL oder kleiner 0 dann Neumessung 
        for i in range (0,100):
            connectionRegistry = QgsApplication.gpsConnectionRegistry()
            connectionList = connectionRegistry.connectionList()
            if connectionList:
                GPSInfo = connectionList[0].currentGPSInformation()
                lat = GPSInfo.latitude
                long = GPSInfo.longitude
                hdop = GPSInfo.hdop
                vdop = GPSInfo.vdop
                elevation = GPSInfo.elevation
                satellitesUsed = GPSInfo.satellitesUsed
                measure = [lat,long,elevation, hdop,vdop,satellitesUsed]
                measures.append(measure)
                #i += 1
                print(lat,long,hdop)
                #sleep(0.6)
        df = pd.DataFrame(measures)            
        result = [str(df[0].mean()),str(df[1].mean()), str(df[2].mean()), 
                  str(df[3].mean()), str(df[4].mean()), str(df[5].mean())]            
        s = ', '.join(result)    
        return s
    else:
        return val0 
        
@qgsfunction(args='auto', group='Custom', referenced_columns=[])
def validate_schichtigkeit(schichtigkeit: int, feature, parent):
    """
    Validates the attribute <i>bestandnschichtigid</i>.
    <h2>Rules:</h2>
    <ul>
      <li>Einschichtig: Nur Bestockung OS und XY. Gesamtsumme der Schichtabdeckung muss 100 oder 200 ergeben.</li>
      <li>Zweischichtig: ...</li>
    </ul>
    <h2>Example usage:</h2>
    <ul>
      <li>validate_schichtigkeit() -> True</li>
    </ul>
    """
    
    RELATION_NAME = 'bestockung'
    ABDECKUNG = 'bestockungbaumartenanteil'
    TYP = 'bestockungschichtid'

    #x = {1: "test", "b": "foo"}
    #QgsMessageLog.logMessage(f"{x}", 'Ausgewähltes Protokoll-Tab', level=Qgis.Info)
    #QgsMessageLog.logMessage("hey")

    relations = QgsProject.instance().relationManager().relationsByName(RELATION_NAME)
    if not relations:
        return False
    relation = relations[0]
    bestockung_features = relation.getRelatedFeatures(feature)
    
    if schichtigkeit is None:
        return False
    """ Es sind immer die Schichtzuordnungen "AB" (36) und "liegend/l" (9) möglich.
        Jede Schicht kann mehrmals angegeben werden, jedoch muss der Baumartenanteil je Schicht addiert genau 100 % ergeben. 
        Ausnahme: Hauptbestand und OS können zusammen addiert 100 % ergeben. """

    liste = list(bestockung_features)
    Hb_OS = sum(f[ABDECKUNG] for f in liste if f[TYP] in [1, 3])
    OS = sum(f[ABDECKUNG] for f in liste if f[TYP] in [3])
    Hb = sum(f[ABDECKUNG] for f in liste if f[TYP] in [1])
    US = sum(f[ABDECKUNG] for f in liste if f[TYP] in [2])
    MH = sum(f[ABDECKUNG] for f in liste if f[TYP] in [25])
    UeH = sum(f[ABDECKUNG] for f in liste if f[TYP] in [31])
    AB = sum(f[ABDECKUNG] for f in liste if f[TYP] in [36])
    P = sum(f[ABDECKUNG] for f in liste if f[TYP] in [0])
    VuS = sum(f[ABDECKUNG] for f in liste if f[TYP] in [4])
    l = sum(f[ABDECKUNG] for f in liste if f[TYP] in [9])

    # QgsMessageLog.logMessage(f"{Hb_OS}", 'Protokoll: Prüfung im Formular', level=Qgis.INFO) 

    if (AB == 0 or AB == 100) and (l == 0 or l == 100): # Es sind immer die Schichtzuordnungen "AB" und "liegend" möglich.

        if schichtigkeit != 6: # nicht plenternartig -> Hauptbestand und OS können zusammen addiert 100 % ergeben.
            if (Hb_OS == 100 or Hb_OS == 200) and (OS <= 100) and (Hb <= 100) and P == 0:
                if schichtigkeit == 1 and US == 0 and MH == 0 and UeH == 0 and VuS == 0: return True # einschichtig, OS (3) und/oder Hauptbestand (1)
                if schichtigkeit == 2 and US == 100 and MH == 0 and UeH == 0 and VuS == 0: return True # zweischichtig, s.o. UND US (2)
                if schichtigkeit == 3 and US == 100 and UeH == 100 and MH == 0 and VuS == 0: return True # zweischichtig mit Überhälter / Nachhiebsrest, s.o. UND US (2) UND UeH (31)
                if schichtigkeit == 4 and VuS == 100 and US == 0 and MH == 0 and UeH == 0: return True # zweischichtig mit Vorausverjüngung, s.o. UND Verjüngung unterm Schirm (4)
                if schichtigkeit == 5 and US == 100 and MH == 0 and UeH == 0 and VuS == 0: return True # zweischichtig mit Unterbau, s.o. UND US (2)
                if schichtigkeit == 7 and US == 100 and MH == 100 and UeH == 0 and VuS == 0: return True # aus Ober-, Mittel- und Unterholz, s.o. UND US (2) UND MH (25)
            else:
                return False

        if schichtigkeit == 6: # mehrschichtig oder plenterartig -> kein Hauptbestand und OS
            if P == 100 and Hb == 0 and OS == 0 and US == 0 and MH == 0 and UeH == 0 and VuS == 0: return True # Plenterwald (0)
            else:
                return False

        else:
            return False

    else:
        return False
